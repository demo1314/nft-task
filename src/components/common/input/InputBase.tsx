import { Input, InputProps } from "antd";
import React from "react";

const styles: Record<string, React.CSSProperties> = {
  Input: {
    height: "32px",
    background: "none",
    border: "1px solid #005D84",
    borderRadius: "2px",
    color: "rgba(255, 255, 255, 0.6)",
  },
};

export interface InputBaseProps extends InputProps {}

export const InputBase = (props: InputBaseProps) => {
  return (
    <Input
      style={styles.Input}
      {...props}
      value={props.value}
      placeholder={props.placeholder}
      onChange={props.onChange}
    />
  );
};

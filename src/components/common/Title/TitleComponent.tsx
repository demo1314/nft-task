import styles from "./Title.module.sass";

export interface BaseTitleProps {
  children: React.ReactNode | string;
}

export const BaseTitle = ({ children }: BaseTitleProps) => {
  return <h1 className={styles.title}>{children}</h1>;
};

import React from "react";
import { Form, FormProps } from "antd";
import {
  FieldErrors,
  FieldValues,
  FormProvider,
  Mode,
  useForm,
  UseFormReturn,
} from "react-hook-form";

export interface RHFormProps<T extends FieldValues = FieldValues>
  extends Omit<FormProps<T>, "onError"> {
  children: React.ReactNode | React.ReactNode[];
  defaultValues?: T;
  initialValues?: T;
  mode?: Mode;
  onFinish: (values: T) => void;
  onError: (errors: FieldErrors<T>) => void;
  methods?: UseFormReturn<T, any>;
}

export const RHForm = <T extends FieldValues>({
  children,
  defaultValues,
  initialValues,
  onFinish,
  onError,
  mode = "onSubmit",
  methods = useForm<T>({
    values: initialValues || defaultValues,
    mode,
  }),
  ...props
}: RHFormProps<T>) => {
  return (
    <FormProvider {...methods}>
      <Form<T>
        {...props}
        onFinish={methods.handleSubmit(onFinish, onError) as any}
      >
        {children}
      </Form>
    </FormProvider>
  );
};

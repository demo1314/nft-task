import { Button } from "antd";
import { ButtonProps } from "antd/lib/button";
import React from "react";

const styles: Record<string, React.CSSProperties> = {
  Button: {
    width: "87px",
    height: "32px",
    background: "#14F1FF",
    border: "none",
    borderRadius: "2px",
  },
};

export interface ButtonBaseProps extends ButtonProps {
  children?: React.ReactNode | string;
}

export const ButtonBase = (props: ButtonBaseProps) => {
  return (
    <Button style={styles.Button} {...props} onClick={props.onClick}>
      {props.children ? props.children : "Continue"}
    </Button>
  );
};

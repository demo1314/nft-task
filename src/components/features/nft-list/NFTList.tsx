import { BaseTitle } from "@/components/common";
import { NFTCollectionCardFetcher } from "@/components/layout";
import { Col, Row } from "antd";
import React from "react";
import { sortByFieldName } from "@/helpers";
import { FetchedNft } from "@/api";

const styles: Record<string, React.CSSProperties> = {
  TitleContainer: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: "86px",
    marginBottom: "30px",
  },
  Counter: {
    color:
      "linear-gradient(272.15deg, #001C1E -21.67%, #006089 21.57%, #00E0EE 62.15%, #EFFCFD 106.06%)",
  },
  Container: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexWrap: "wrap",
  },
};
export interface NFTListProps {
  nfts: FetchedNft[];
  loading?: boolean;
}

export const NFTList = (props: NFTListProps) => {
  const createCollections = (array: FetchedNft[]) => {
    const collections: FetchedNft[][] = [];
    let addr = "";
    let arr: FetchedNft[] = [];
    array.map((nft, idx: number) => {
      if (idx === 0) {
        addr = nft.updateAuthority;
        arr.push(nft);
      } else if (idx === array.length - 1) {
        arr.push(nft);
        collections.push(arr);
      } else {
        if (addr == nft.updateAuthority) {
          arr.push(nft);
        }
        if (addr !== nft.updateAuthority) {
          collections.push(arr);
          arr = [];
          addr = nft.updateAuthority;
          arr.push(nft);
        }
      }
    });
    return collections;
  };
  const collections = createCollections(
    sortByFieldName(props.nfts, "updateAuthority")
  );

  const { loading = false } = props;
  return (
    <Row>
      <Col span={18} offset={4} style={styles.TitleContainer}>
        {loading ? (
          <BaseTitle>Wait a bit…</BaseTitle>
        ) : (
          <BaseTitle>
            My collections{" "}
            <span style={styles.Counter}>{collections.length}</span>
          </BaseTitle>
        )}
      </Col>
      <Col span={18} offset={4} style={styles.Container}>
        {collections.map((collection: any, idx: number) => {
          return <NFTCollectionCardFetcher key={idx} collection={collection} />;
        })}
      </Col>
    </Row>
  );
};

import { Col, message, Row } from "antd";
import { useForm } from "react-hook-form";
import { useMutation } from "@tanstack/react-query";
import { nftApiService } from "@/api";
import { RHForm } from "@/components/common";
import { SearchLayout } from "@/components/layout";

const styles: Record<string, React.CSSProperties> = {
  Container: { marginTop: "48px" },
};

export interface NFTSearchProps {
  onData: (data: any) => void;
}

export interface NFTSearchForm {
  address: string;
}

const exampleAddress = "8PdqmeKdn3999sT3jkkx3JRquGqZAfr3m7F4G5NoWkuG";

export const NFTSearch = (props: NFTSearchProps) => {
  const { mutate, isLoading } = useMutation(
    ({ address }: NFTSearchForm) => {
      return nftApiService.search(address);
    },
    {
      onSuccess: (result) => {
        props.onData(result!.data.data);
        message.success("Searching success");
      },
      onError: (err) => {
        console.log("onError: ", err);
        message.error("Searching error");
      },
    }
  );

  const methods = useForm<NFTSearchForm>({
    defaultValues: {
      address: "",
    },
    mode: "all",
  });

  const handleSearch = async (value: NFTSearchForm) => {
    const address = value.address;
    mutate({ address });
  };

  return (
    <Row style={styles.Container}>
      <Col span={12} offset={6}>
        <RHForm
          methods={methods}
          onFinish={handleSearch}
          onError={(err) => console.log("error: ", err)}
        >
          <SearchLayout loading={isLoading} />
        </RHForm>
        <br />
        <p>{exampleAddress}</p>
      </Col>
    </Row>
  );
};

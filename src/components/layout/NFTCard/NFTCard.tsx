import React, { useState } from "react";
import { SearchOutlined } from "@ant-design/icons";
import { ButtonBase, InputBase } from "@/components";
import classes from "./NFTCard.module.sass";
import { Card } from "antd";
import Image, { ImageProps } from "next/image";
import { useQuery } from "@tanstack/react-query";

const styles: Record<string, React.CSSProperties> = {
  Card: {
    maxWidth: "325px",
    minWidth: "325px",
    maxHeight: "389px",
    minHeight: "389px",
    borderImage: "linear-gradient(to bottom, #005D84, #84007F) 1",
    borderRadius: "2px",
    background: "none",
    margin: "12px",
  },
  Image: {
    maxWidth: "325px",
    maxHeight: "325px",
  },
  CardBody: {
    padding: 0,
  },
};

const { Meta } = Card;

export interface SearchLayoutProps {
  url: string;
  loading?: boolean;
}

export const NFTCard = (props: SearchLayoutProps) => {
  const { data } = useQuery({
    queryKey: ["groups"],
    queryFn: () => fetch(props.url),
  });
  console.log("fetched image info: ", data);
  return (
    <Card
      style={styles.Card}
      bodyStyle={styles.CardBody}
      cover={
        <picture>
          <img className={classes.image} alt="example" src={props.url} />
        </picture>
      }
    >
      <Meta title="Europe Street beat" description="www.instagram.com" />
    </Card>
  );
};

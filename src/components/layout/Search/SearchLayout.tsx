import { SearchOutlined } from "@ant-design/icons";
import { ButtonBase } from "@/components";
import classes from "./Search.module.sass";
import { Controller } from "react-hook-form";

const styles: Record<string, React.CSSProperties> = {
  Input: {
    width: "100%",
    height: "32px",
    background: "none",
    border: "none",
    color: "rgba(255, 255, 255, 0.6)",
    marginLeft: "10px",
  },
  IconSearch: {
    color: "rgba(255, 255, 255, 0.6)",
    fontSize: "20px",
  },
};

export interface SearchLayoutProps {
  loading?: boolean;
}

export const SearchLayout = (props: SearchLayoutProps) => {
  return (
    <div className={classes.searchContainer}>
      <Controller
        name={"address"}
        render={({ field: { onChange, value, ref } }) => {
          return (
            <>
              <div className={classes.inputBox}>
                <SearchOutlined style={styles.IconSearch} />
                <input
                  className={classes.input}
                  value={value}
                  onChange={onChange}
                  placeholder={"Search by address"}
                  ref={ref}
                />
              </div>
              <ButtonBase htmlType="submit" loading={props.loading}>
                Search
              </ButtonBase>
            </>
          );
        }}
      ></Controller>
    </div>
  );
};

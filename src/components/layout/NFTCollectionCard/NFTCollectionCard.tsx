import React from "react";
import { Card, Col, Row, Typography } from "antd";
import { ArrowRightOutlined } from "@ant-design/icons";
import classes from "./NFTCollectionCard.module.sass";

const styles: Record<string, React.CSSProperties> = {
  Card: {
    maxWidth: "325px",
    minWidth: "325px",
    maxHeight: "389px",
    minHeight: "389px",
    background: "none",
    borderImage: "linear-gradient(to bottom, #005D84, #84007F) 1",
    marginBottom: "24px",
    marginRight: "24px",
    borderRadius: "2px",
  },
  CardFill: {
    maxWidth: "325px",
    minWidth: "325px",
    maxHeight: "387px",
    minHeight: "387px",
    border: "none",
    marginBottom: "24px",
    marginRight: "24px",
    background: "none",
  },
  Image: {
    maxWidth: "325px",
    maxHeight: "325px",
  },
  CardBody: {
    padding: 0,
    height: "64px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    background: "#01122A",
  },
};

const { Text } = Typography;

export interface NFTCollectionCardProps {
  data: Collection;
  loading?: boolean;
}

export interface Collection {
  collection?: { name: string };
  image?: string;
  name?: string;
  symbol?: string;
}

export const NFTCollectionCard = (props: NFTCollectionCardProps) => {
  const { data } = props;
  return (
    <Card
      loading={props.loading}
      style={styles.CardFill}
      bodyStyle={styles.CardBody}
      cover={
        <picture style={{ display: "flex", justifyContent: "center" }}>
          <img className={classes.image} alt="example" src={data.image} />
        </picture>
      }
    >
      {
        <Row style={{ width: "100%" }}>
          <Col span={20} offset={1}>
            <Text style={{ color: "#FFFFFF" }}>
              {data.collection?.name || data?.symbol || "Uncollected"}
            </Text>
          </Col>
          <Col span={1}>
            <ArrowRightOutlined style={{ color: "#14F1FF" }} />
          </Col>
        </Row>
      }
    </Card>
  );
};

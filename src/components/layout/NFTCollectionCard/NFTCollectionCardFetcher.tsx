import React, { useEffect, useState } from "react";
import classes from "./NFTCollectionCard.module.sass";
import { Card, Col, message, Row, Typography } from "antd";
import axios, { AxiosResponse } from "axios";
import { FetchedNft, nftApiService } from "@/api";
import { ArrowRightOutlined } from "@ant-design/icons";
import { useQuery } from "@tanstack/react-query";
import { NFTCollectionCard, NFTCollectionCardProps } from "./NFTCollectionCard";

const styles: Record<string, React.CSSProperties> = {
  Card: {
    maxWidth: "325px",
    minWidth: "325px",
    maxHeight: "389px",
    minHeight: "389px",
    background: "none",
    borderImage: "linear-gradient(to bottom, #005D84, #84007F) 1",
    marginBottom: "24px",
    marginRight: "24px",
    borderRadius: "2px",
  },
  CardFill: {
    maxWidth: "325px",
    minWidth: "325px",
    maxHeight: "387px",
    minHeight: "387px",
    border: "none",
    marginBottom: "24px",
    marginRight: "24px",
    background: "none",
  },
  Image: {
    maxWidth: "325px",
    maxHeight: "325px",
  },
  CardBody: {
    padding: 0,
    height: "64px",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    background: "#01122A",
  },
};

const { Text } = Typography;

export interface NFTCollectionCardFetcherProps {
  collection: FetchedNft[];
  loading?: boolean;
}

export const NFTCollectionCardFetcher = (
  props: NFTCollectionCardFetcherProps
) => {
  const { data, isLoading, error } = useQuery(
    [`fetchCollection - ${props.collection[0].updateAuthority}`],
    {
      queryFn: () => {
        const param = props.collection[0];
        return nftApiService.fetch(
          param.address,
          param.uri,
          param.updateAuthority
        );
      },
    }
  );
  if (error) return <p>{JSON.stringify(error)}</p>;
  if (isLoading) return <Card style={styles.Card} />;
  return (
    <>
      {data && (
        <NFTCollectionCard
          data={data.data.data.metadata as NFTCollectionCardProps["data"]}
          loading={isLoading}
        />
      )}
    </>
  );
};

import React, { useState } from "react";
import { BaseTitle } from "@/components";
import { NFTList, NFTSearch } from "@/components/features";
import { ResponseNFT } from "@/api";

const styles: Record<string, React.CSSProperties> = {
  Container: { width: "100%", paddingTop: "117px" },
};

export const SearchedNftList = () => {
  const [fetchedData, setFetchedDAta] = useState<ResponseNFT>();

  return (
    <div style={styles.Container}>
      <BaseTitle>
        Search <span>NFT-collection</span> by address
      </BaseTitle>
      <NFTSearch onData={(data) => setFetchedDAta(data)} />
      {fetchedData && <NFTList nfts={fetchedData.nfts} />}
    </div>
  );
};

import {theme} from 'antd';
import {ThemeConfig} from 'antd/lib/config-provider/context';
import {Nunito_Sans} from '@next/font/google';

const font = Nunito_Sans({
  subsets: ['latin', 'cyrillic'],
  weight: ['200', '400', '600', '900'],
});

export const defaultTheme: ThemeConfig = {
  token: {
    ...font.style,
    marginLG: 8,
  },
  algorithm: [theme.defaultAlgorithm],
  components: {
    Typography: {
      colorPrimary: '#F5F8F9',
      colorSuccess: '#52c41a',
      colorLink: '#40A9FF',
      lineHeight: 1.35,
    },
    Button: {
      borderRadius: 16,
      controlHeight: 48,
      fontSize: 16,
    },
    Input: {
      borderRadius: 8,
      controlHeight: 56,
      fontSize: 17,
    },
    Select: {
      controlHeight: 56,
    },
    DatePicker: {
      controlHeight: 12,
    },
  },
};

export const darkTheme: ThemeConfig = {
  algorithm: [theme.darkAlgorithm],
  token: {
    ...font.style,
    colorBgContainer: '#242A32',
    colorBgLayout: '#171C20',
    colorPrimary: '#1890FF',
    colorTextPlaceholder: '#87929F',
    colorTextLabel: '#87929F',
    colorBorder: 'rgba(0,0,0,0)',
    marginLG: 8,
  },
  components: {
    Typography: {
      colorSuccess: '#2CD7AA',
      colorLink: '#40A9FF',
      lineHeight: 1.35,
    },
    Button: {
      borderRadius: 16,
      controlHeight: 48,
      colorBgBase: 'linear-gradient(180deg, #5C47DB 0%, #4C40D3 100%)',
      fontSize: 16,
    },
    Input: {
      colorPrimary: 'red',
      borderRadius: 8,
      controlHeight: 56,
      colorTextBase: 'red',
      fontSize: 17,
    },
    InputNumber: {
      colorPrimary: 'red',
      borderRadius: 8,
      controlHeight: 56,
      colorTextBase: 'red',
      fontSize: 17,
    },
    Select: {
      controlHeight: 52,
    },
    DatePicker: {
      controlHeight: 12,
    },
  },
};

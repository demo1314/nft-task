export const sortByFieldName = <T extends any>(
  array: T[],
  fieldName: keyof T
): T[] => {
  return array.sort((a, b) => {
    if (a[fieldName] > b[fieldName]) return 1;
    if (a[fieldName] < b[fieldName]) return -1;
    return 0;
  });
};

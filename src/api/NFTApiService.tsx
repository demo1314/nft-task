import axios from "axios";

export interface FetchedNft {
  address: string; // "83hCoxBRqdMEKxJvbF5S4hwNQ8W9fJZ8imZMv28zbcwt"
  creators: [{ address: string; verified: number; share: number }];
  isMutable: number; // 1
  key: number; // 4
  name: string; // "ShroomZNFT 2163"
  primarySaleHappened: number; // 1,
  sellerFeeBasisPoints: 500;
  symbol: string; // "ShroomZNFT",
  updateAuthority: string; //"6NYiuj5wUdPUB7dmXvU7qf8kVuekMADeSvQsTqxKR2zb",
  uri: string; // "https://arweave.net/hb30mojNeCFTsRPnG0Jc9LZew9uvSoXaTDrofu2rDbc",
}

export interface ResponseNFT {
  blockchain: "solana";
  nfts: FetchedNft[];
}

class NFTApiService {
  private serviceName = "NFTApiService";

  async search(address: string) {
    try {
      return await axios.get(
        `https://dev.solhall.io/v1/nft/solana/address/${address}`
      );
    } catch (err) {
      console.error(this.serviceName + "search error: ", err);
    }
  }

  async fetch(address: string, uri: string, updateAuthority: string) {
    try {
      return await axios.get(
        `https://dev.solhall.io/v1/nft/solana/metadata/?nftAddress=${address}&url=${uri}&holderAddress=${updateAuthority}`
      );
    } catch (error) {
      console.error(this.serviceName + "fetch error: ", error);
    }
  }
}

export const nftApiService = new NFTApiService();
